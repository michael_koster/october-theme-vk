/**
 * Bootstrap - Starter Kit
 *
 * @author Prismify
 * @version 1.0.5
 */

window.$ = window.jQuery = require('jquery');
require('popper.js');
require('bootstrap');
require('@fancyapps/fancybox');

// require('more-packages-installed-with-npm-install');

$(function() {
    "use strict";

    // Helpers
    require('./helpers/bootstrap');
    // require('.helpers/more-helpers');

    // Components
    // require('./components/more-components');

    // Modules
    require('../../../../modules/system/assets/js/framework');
    require('../../../../modules/system/assets/js/framework.extras');


    $(document).ready(function() {
        $('[data-fancybox="gallery"]').fancybox({
            buttons: [
                "close"
              ],
        });
    })

    window.onscroll = function() {
        const logo = document.getElementById('logo');
        if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
            logo.style.height = '60px';
        }
        else {
            logo.style.height = '100px';
        }
    }

});